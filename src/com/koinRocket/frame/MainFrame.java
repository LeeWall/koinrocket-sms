package com.koinRocket.frame;

import com.koinRocket.manager.SerialPortManager;
import com.koinRocket.util.ByteUtils;
import com.koinRocket.util.ShowUtils;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainFrame extends JFrame implements ActionListener {

    public static final Logger LOGGER = LoggerFactory.getLogger(MainFrame.class);

    public String format = "yyyy-MM-dd HH:mm:ss";

    //程序界面宽度
    public static final int WIDTH = 500;
    //程序界面高度
    public static final int HEIGHT = 360;

    private JTextPane dataView = new JTextPane();
    private JScrollPane scrollDataView = new JScrollPane(dataView);


    /**
     * 串口设置面板
     */
    private JPanel serialPortPanel = new JPanel();
    private JLabel serialPortLabel = new JLabel("串口");
    private JLabel baudrateLabel = new JLabel("波特率");
    private JComboBox commChoice = new JComboBox();
    private JComboBox baudrateChoice = new JComboBox();


    /**
     * 串口操作面板
     */
    private JPanel operatePanel = new JPanel();
    private JTextArea dataInput = new JTextArea();
    private JButton serialPortOpenBtn = new JButton("打开串口");
    private JButton sendDataBtn = new JButton("发送数据");
    private JButton openCmdButton = new JButton("打开射频");
    private JButton closeCmdButton = new JButton("关闭射频");


    /**
     * 正常的风格
     */
    private final String STYLE_NORMAL = "normal";
    /**
     * 字体为红色
     */
    private final String STYLE_RED = "red";


    private List<String> commList = null;
    private SerialPort serialport;


    public MainFrame() {
        initView();
        initComponents();
        initData();
    }


    private void initView() {
        // 关闭程序
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        // 禁止窗口最大化
        setResizable(false);
        // 设置程序窗口居中显示
        Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
        setBounds(p.x - WIDTH / 2, p.y - HEIGHT / 2, 499, 455);
        getContentPane().setLayout(null);
        setTitle("串口通讯");
    }


    private void initComponents() {
        // 数据显示
        dataView.setFocusable(false);
        scrollDataView.setBounds(10, 10, 475, 200);
        //数据区域的风格
        Style def = dataView.getStyledDocument().addStyle(null, null);
        StyleConstants.setFontFamily(def, "verdana");
        StyleConstants.setFontSize(def, 12);
        Style normal = dataView.addStyle(STYLE_NORMAL, def);
        Style s = dataView.addStyle(STYLE_RED, normal);
        StyleConstants.setForeground(s, Color.RED);
        dataView.setParagraphAttributes(normal, true);
        getContentPane().add(scrollDataView);


        // 串口设置
        serialPortPanel.setBorder(BorderFactory.createTitledBorder("串口设置"));
        serialPortPanel.setBounds(10, 220, 170, 188);
        serialPortPanel.setLayout(null);
        getContentPane().add(serialPortPanel);


        serialPortLabel.setForeground(Color.gray);
        serialPortLabel.setBounds(10, 25, 40, 20);
        serialPortPanel.add(serialPortLabel);


        commChoice.setFocusable(false);
        commChoice.setBounds(60, 25, 100, 20);
        serialPortPanel.add(commChoice);


        baudrateLabel.setForeground(Color.gray);
        baudrateLabel.setBounds(10, 60, 40, 20);
        serialPortPanel.add(baudrateLabel);


        baudrateChoice.setFocusable(false);
        baudrateChoice.setBounds(60, 60, 100, 20);
        serialPortPanel.add(baudrateChoice);


        // 操作
        operatePanel.setBorder(BorderFactory.createTitledBorder("操作"));
        operatePanel.setBounds(200, 220, 285, 188);
        operatePanel.setLayout(null);
        getContentPane().add(operatePanel);


        dataInput.setBounds(25, 25, 235, 63);
        operatePanel.add(dataInput);


        serialPortOpenBtn.setFocusable(false);
        serialPortOpenBtn.setBounds(45, 98, 90, 20);
        serialPortOpenBtn.addActionListener(this);
        operatePanel.add(serialPortOpenBtn);


        sendDataBtn.setFocusable(false);
        sendDataBtn.setBounds(155, 98, 90, 20);
        sendDataBtn.addActionListener(this);
        operatePanel.add(sendDataBtn);


        openCmdButton.setBounds(45, 128, 90, 20);
        openCmdButton.addActionListener(this);
        operatePanel.add(openCmdButton);


        closeCmdButton.setBounds(155, 128, 90, 20);
        closeCmdButton.addActionListener(this);
        operatePanel.add(closeCmdButton);


        JButton btnNewButton = new JButton("clear");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dataView.setText("");
            }
        });
        btnNewButton.setBounds(45, 158, 90, 20);
        operatePanel.add(btnNewButton);
        
    }


    /**
     *
     */
    private void initData() {
        commList = SerialPortManager.findPorts();
        // 检查是否有可用串口，有则加入选项中
        if (commList == null || commList.size() < 1) {
            ShowUtils.warningMessage("没有搜索到有效串口！");
        } else {
            for (String s : commList) {
                commChoice.addItem(s);
            }
        }
        baudrateChoice.addItem("9600");
        baudrateChoice.addItem("19200");
        baudrateChoice.addItem("38400");
        baudrateChoice.addItem("57600");
        baudrateChoice.addItem("115200");
    }




    /**
     * 打开串口
     * 
     */
    private void openSerialPort() {
        // 获取串口名称
        String commName = (String) commChoice.getSelectedItem();
        // 获取波特率
        int baudrate = 9600;
        String bps = (String) baudrateChoice.getSelectedItem();
        baudrate = Integer.parseInt(bps);
        // 检查串口名称是否获取正确
        if (commName == null || commName.equals("")) {
            ShowUtils.warningMessage("没有搜索到有效串口！");
        }
        else {
            serialport = SerialPortManager.openPort(commName, baudrate);
            if (serialport != null) {
                dataShow("串口已打开", STYLE_RED);
                serialPortOpenBtn.setText("关闭串口");
            }
        }
        SerialPortManager.addListener(serialport, new SerialListener(serialport));

    }

    /**
     * 关闭串口
     * 
     */
    private void closeSerialPort() {
        SerialPortManager.closePort(serialport);
        dataShow("已经关闭串口",STYLE_RED);
        serialPortOpenBtn.setText("打开串口");
        serialport = null;
    }

    /**
     * 打印数据到面板上
     * 
     */
    private void dataShow(String text,String style) {
        StringBuilder builderData = new StringBuilder();
        builderData.setLength(0);
        builderData.append(new SimpleDateFormat(format).format(new Date())).append("\r\n").append(text).append("\r\n");
        try {
            Document document = dataView.getDocument();
            if(STYLE_RED.equals(style)){
                dataView.getDocument().insertString(document.getLength(), builderData.toString(), dataView.getStyle(style));
            }else{
                dataView.getDocument().insertString(document.getLength(), builderData.toString(), dataView.getStyle(STYLE_NORMAL));
            }
            dataView.setCaretPosition(document.getLength());
        } catch (BadLocationException e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * 发送指定数据
     * 
     */
    private void sendData(String data) {
        try {
            if (serialport != null){
                SerialPortManager.sendToPort(serialport, ByteUtils.hexStr2Byte(data));
                dataShow(data,STYLE_RED);
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == serialPortOpenBtn) {
            if (serialport == null) {
                openSerialPort();
            } else {
                closeSerialPort();
            }
        } else if (e.getSource() == sendDataBtn) {
            String data = dataInput.getText().toString();
            if(data == null || "".equals(data)){
                return;
            }
            sendData(data);
        } else if (e.getSource() == openCmdButton) {
            // 0x02 0x05 0x01 0xaa 0x03 --> "0x02 0x05 0x01 0x06 0x03"
            String data = "020501aa03";
            sendData(data);
        } else if (e.getSource() == closeCmdButton) {
            // 0x02 0x05 0x02 0xaa 0x03 --> "0x02 0x05 0x02 0x05 0x03"
            String data = "020502aa03";
            sendData(data);
        }
    }

    class SerialListener implements SerialPortEventListener{

        private SerialPort serialPort;

        public SerialListener(SerialPort serialPort) {
            this.serialPort = serialPort;
        }

        @Override
        public void serialEvent(SerialPortEvent serialPortEvent) {
            byte[] data = null;
            switch (serialPortEvent.getEventType()) {
                case SerialPortEvent.BI: // 10 通讯中断
                    ShowUtils.errorMessage("与串口设备通讯中断");
                    break;
                case SerialPortEvent.FE: // 9 帧错误
                case SerialPortEvent.PE: // 8 奇偶校验错误
                case SerialPortEvent.OE: // 7 溢位（溢出）错误
                case SerialPortEvent.CD: // 6 载波检测
                case SerialPortEvent.RI: // 5 振铃指示
                case SerialPortEvent.CTS: // 3 清除待发送数据
                case SerialPortEvent.DSR: // 4 待发送数据准备好了
                case SerialPortEvent.OUTPUT_BUFFER_EMPTY: // 2 输出缓冲区已清空
                    ShowUtils.warningMessage("输出缓冲区已清空");
                    break;
                case SerialPortEvent.DATA_AVAILABLE: // 1 串口存在可用数据
                    try {
                        if (serialPort == null) {
                            ShowUtils.errorMessage("串口对象为空！监听失败！");
                        }else {
                            // 读取串口数据
                            data = SerialPortManager.readFromPort(serialPort);
                            dataShow(ByteUtils.byteArrayToHexString(data), STYLE_NORMAL);
                        }
                    } catch (Exception e) {
                        ShowUtils.errorMessage(e.toString());
                        LOGGER.error(e.getMessage());
                    }
                    break;
            }
        }
    }

}
