package com.koinRocket.util;

import javax.swing.JOptionPane;

/**
 * 提示框
 */
public class ShowUtils {


	/**
	 * 消息提示
	 * @param message
	 */
	public static void message(String message) {
		JOptionPane.showMessageDialog(null, message);
	}


	/**
	 * 警告提示
	 * @param message
	 */
	public static void warningMessage(String message) {
		JOptionPane.showMessageDialog(null, message, "警告",
				JOptionPane.WARNING_MESSAGE);
	}


	/**
	 * 错误提示
	 * @param message
	 */
	public static void errorMessage(String message) {
		JOptionPane.showMessageDialog(null, message, "错误",
				JOptionPane.ERROR_MESSAGE);
	}


	/**
	 * 自定义提示
	 * @param title
	 * @param message
	 */
	public static void plainMessage(String title, String message) {
		JOptionPane.showMessageDialog(null, message, title,
				JOptionPane.PLAIN_MESSAGE);
	}


	/**
	 * 带有选择功能提示
	 * @param title
	 * @param message
	 * @return
	 */
	public static boolean selectMessage(String title, String message) {
		int isConfirm = JOptionPane.showConfirmDialog(null, message, title,
				JOptionPane.YES_NO_OPTION);
		if (0 == isConfirm) {
			return true;
		}
		return false;
	}
}
