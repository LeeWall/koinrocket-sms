package com.koinRocket.test;

import org.smslib.AGateway;
import org.smslib.Message;
import org.smslib.OutboundMessage;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;

public class SMSDemo  {

    public void send(String id , String serialPort, int boaurate, String mauctuer, String model, String number) throws Exception{
        SerialModemGateway gateway = new SerialModemGateway(id, serialPort, boaurate, mauctuer, model);
        gateway.setInbound(true);
        gateway.setOutbound(true);
        gateway.setProtocol(AGateway.Protocols.PDU);
        Service service = Service.getInstance();
        service.addGateway(gateway);
        service.startService();
        OutboundMessage message = new OutboundMessage(number, "Hello World");
        message.setEncoding(Message.MessageEncodings.ENCUCS2);
        service.sendMessage(message);
        service.stopService();
    }

}
