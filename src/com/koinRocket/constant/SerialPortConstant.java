package com.koinRocket.constant;

public class SerialPortConstant {

    public static final String PORT_I2C = "I2C";
    public static final String PORT_PARALLEL = "Parallel";
    public static final String PORT_RAW = "Raw";
    public static final String PORT_RS485 = "RS485";
    public static final String PORT_SERIAL = "Serial";
    public static final String UNKNOWN_TYPE = "Unknown Type";
}
