package com.koinRocket;

import com.koinRocket.frame.MainFrame;

import java.awt.*;

public class Start  {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
}
