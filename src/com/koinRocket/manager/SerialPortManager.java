package com.koinRocket.manager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.TooManyListenersException;
import com.koinRocket.constant.SerialPortConstant;
import com.koinRocket.util.ArrayUtils;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *  串口管理类
 */
public class SerialPortManager {

	public static final Logger LOGGER = LoggerFactory.getLogger(SerialPortManager.class);

	/**
	 * 查找所有可用端口
	 * 
	 * @return 可用端口名称列表
	 */
	public static final ArrayList<String> findPorts() {
		// 获得当前所有可用串口
		Enumeration<CommPortIdentifier> portList = CommPortIdentifier.getPortIdentifiers();
		ArrayList<String> portNameList = new ArrayList<String>();
		while (portList.hasMoreElements()) {
			CommPortIdentifier commPortIdentifier = portList.nextElement();
			String portName = commPortIdentifier.getName();
			int portType = commPortIdentifier.getPortType();
			LOGGER.info("portName:[{}], portType:[{}]", portName, getPortTypeName(portType));
			portNameList.add(portName);
		}
		return portNameList;
	}

	/**
	 * 获取通信接口类型名称
	 * @param portType
	 * @return
	 */
	public static String getPortTypeName(int portType){
		switch (portType){
			case CommPortIdentifier.PORT_I2C:
				return SerialPortConstant.PORT_I2C;
			case CommPortIdentifier.PORT_PARALLEL:
				return SerialPortConstant.PORT_PARALLEL;
			case CommPortIdentifier.PORT_RAW:
				return SerialPortConstant.PORT_RAW;
			case CommPortIdentifier.PORT_RS485:
				return SerialPortConstant.PORT_RS485;
			case CommPortIdentifier.PORT_SERIAL:
				return SerialPortConstant.PORT_SERIAL;
			default:
				return SerialPortConstant.UNKNOWN_TYPE;
		}
	}

	/**
	 * 开启串口
	 * @param portName
	 * @param baudrate
	 * @return
	 * @throws PortInUseException
	 */
	public static final SerialPort openPort(String portName, int baudrate) {
		try {
			// 通过端口名识别端口
			CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
			// 打开端口和开启超时时间
			CommPort commPort = portIdentifier.open(portName, 2000);
			// 判断是否是串口
			if (commPort instanceof SerialPort) {
				SerialPort serialPort = (SerialPort) commPort;
				//设置波特率， 数据位， 停止位， 校验位
				serialPort.setSerialPortParams(baudrate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
				return serialPort;
			}
		} catch (NoSuchPortException e1) {
			LOGGER.error("没有这样的串口异常",e1);
		} catch (PortInUseException e) {
			LOGGER.error("串口被占用异常",e);
		} catch (UnsupportedCommOperationException e) {
			LOGGER.error("不被支持操作异常",e);
		}
		return null;
	}

	/**
	 * 关闭串口
	 * 
	 * @param serialPort
	 *
	 */
	public static void closePort(SerialPort serialPort) {
		if (serialPort != null) {
			serialPort.close();
		}
	}

	/**
	 * 往串口发送数据
	 * @param serialPort
	 * @param order
	 */
	public static void 	sendToPort(SerialPort serialPort, byte[] order) {
		OutputStream out = null;
		try {
			out = serialPort.getOutputStream();
			out.write(order);
			out.flush();
		} catch (IOException e) {
			LOGGER.error("发送数据到串口IO异常",e);
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				LOGGER.error("输出流关闭异常",e);
			}
		}
	}

	/**
	 * 从串口读取消息
	 * @param serialPort
	 * @return
	 */
	public static byte[] readFromPort(SerialPort serialPort) {
		InputStream in = null;
		byte[] bytes = {};
		try {
			in = serialPort.getInputStream();
			byte[] readBuffer = new byte[1];
			int bytesNum = in.read(readBuffer);
			while (bytesNum > 0) {
				bytes = ArrayUtils.concat(bytes, readBuffer);
				bytesNum = in.read(readBuffer);
			}
		} catch (IOException e) {
			LOGGER.error("读串口消息IO异常",e);
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				LOGGER.error("流关闭异常",e);
			}
		}
		return bytes;
	}

	/**
	 * 监听串口
	 * @param serialPort
	 * @param listener
	 */
	public static void addListener(SerialPort serialPort, SerialPortEventListener listener) {
		try {
			// 设置串口监听
			serialPort.addEventListener(listener);
			// 设置串口数据时间有效
			serialPort.notifyOnDataAvailable(true);
			// 设置党同心中断时唤醒中断线程
			serialPort.notifyOnBreakInterrupt(true);
		} catch (TooManyListenersException e) {
			LOGGER.error("串口监听异常",e);
		}
	}

}
